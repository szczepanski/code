import re
import pickle  # binaries
import sys  # used here to capture parsed in variables - JSON
import json

# dummy output form show
comm1_out = """Router# show ip interface brief
Interface             IP-Address      OK?    Method Status     	Protocol
GigabitEthernet0/1    unassigned      YES    unset  up         	up
GigabitEthernet0/2    192.168.190.235 YES    unset  up         	up
TenGigabitEthernet2/1 unassigned      YES    unset  DOWN        DOWN
Te36/46               unassigned      YES    unset  down       	down
Te36/47               unassigned      YES    unset  down       	down
Te36/48               unassigned      YES    unset  down       	down
Virtual36             unassigned      YES    unset  up         	up
The following table describes the significant fields shown in the display."""

# python command run on ipmon : ### python net_18.py '{'mode': 0, 'hostname': 'test.ho'}' ###

# capture parsed in json from the command above
# sys.argv.append('{"mode": 0, "hostname": "bbp-test-switch-1.ho", "ip_address": "10.10.10.10"}')
sys.argv.append('{"mode": 1, "hostname": "bbp-test-switch-2.ho", "ip_address": "10.10.10.20"}')

bin_location = "net_18.bin"

# try to load input JSON (verify input)
try:
    input_json = json.loads(sys.argv[1])
    mode = input_json["mode"]
    hostname = input_json["hostname"]
    ip_address = input_json["ip_address"]
except (IndexError, TypeError, KeyError):
    print(json.dumps({"status": 1, "output": "failed to load json input"}))
    quit(1)

    """
# check if bin file (base file) already exists, else create blank dictionary
try:
    with open(bin_location, "rb") as f:
        net_18 = pickle.load(f)
        print net_18

except IOError as e:
    net_18 = {}
    """

regex = re.compile(r"(?P<name>^\S+\d).+(?P<state>(up)|(down)$)", re.MULTILINE | re.IGNORECASE)
matches = regex.finditer(comm1_out)

# check if the output contains interface information

"""if regex.search(comm1_out):

    net_18[hostname] = {}
    for match in regex.finditer(comm1_out):
        interface = match.groupdict()["name"]
        state = match.groupdict()["state"]
        net_18[hostname][interface] = state
        # print("Interface Name: {}\nInterface State: {}".format(match.groupdict()["name"], match.groupdict()["state"]))
else:
    exit(500)
print(net_18)
"""

#with open(bin_location, "wb") as f:
#    pickle.dump(net_18, f)




# main
#######################################

def main():
    # check if bin file (base file) already exists, else create blank dictionary

    try:
        with open(bin_location, "rb") as f:
            net_18 = pickle.load(f)

    except IOError as e:
        net_18 = {}

    input_json = json.loads(sys.argv[1])
    mode = input_json["mode"]
    hostname = input_json["hostname"]
    ip_address = input_json["ip_address"]

    if mode == 0:  # create new baseline
        print('Run in mode 0')

        if regex.search(comm1_out):

            net_18[hostname] = {}
            for match in regex.finditer(comm1_out):
                interface = match.groupdict()["name"]
                state = match.groupdict()["state"]
                net_18[hostname][interface] = state
                # print("Interface Name: {}\nInterface State: {}".format(match.groupdict()["name"], match.groupdict(
                # )["state"]))
            print net_18

    elif mode == 1:
        # compare baseline with current reading
        print('Run in mode 1')
        # get current reading
        for match in regex.finditer(comm1_out):
            interface = match.groupdict()["name"]
            state = match.groupdict()["state"]
            net_18[hostname][interface] = state
            # print("Interface Name: {}\nInterface State: {}".format(match.groupdict()["name"], match.groupdict(
            # )["state"]))
        print "- Current state:"
        print net_18

        # get baseline
        try:
            with open(bin_location, "rb") as f:
                net_18_base = pickle.load(f)
                print "- Baseline state:"
                print net_18_base
                # compare net18 against net_18_base
                print '- Comparison below: '
                match = True
                for key in net_18_base:
                    if net_18_base[key] != net_18[key]:
                        match = False
                        print net_18_base[key],
                        print  '->',
                        print net_18[key]


        except IOError as e:
            # net_18 = {}
            print "Missing Baseline"




"""for keys in dict1:
    if dict1[keys] != dict2[keys]:
        match = False
        print keys
        print dict1[keys],
        print  '->' ,
        print dict2[keys]"""


"""
 """"""""""""




    if "waitTime" in input_json:
        wait = float(input_json["waitTime"])
    else:
        wait = float(4)
    if "enable" in input_json:
        enable = input_json["enable"]
    else:
        enable = False
    if "enablePassword" in input_json:
        enable_password = input_json["enablePassword"]
    else:
        enable_password = None
    if "promptChar" in input_json:
        prompt_char = input_json["promptChar"]
    else:
        prompt_char = None
    #
    if mode in ("e", "s", "si"):
        connection = SSHHandler(hostname, command, mode, enable=enable, wait=wait,
                                enable_password=enable_password, prompt_char=prompt_char)
    elif mode in ("es", "esi"):
        connection = SSHExpectHandler(hostname, command, mode, enable=enable, wait=wait,
                                      enable_password=enable_password, prompt_char=prompt_char)
    else:
        print(json.dumps({"status": 206, "output": "Mode is invalid."}))
        exit(206)
    #
    connection.verify_credentials()
    if connection.out_json["status"] != 0:
        print(json.dumps(connection.out_json))
        exit(connection.out_json["status"])
    if mode == "e":
        connection.exec_command()
    elif mode in ("s", "si", "es", "esi"):
        connection.shell()
    print(json.dumps(connection.out_json))
    exit(connection.out_json["status"])

"""

if __name__ == "__main__":  # parse json if run via directly
    main()
